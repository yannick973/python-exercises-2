# Python Exercises

Clone the directory using git with the command `git clone https://thekswenson@bitbucket.org/thekswenson/python-exercises.git`.

The exercises can be found at `text/programmingexercises.pdf`.
You should complete the various function definitions for exercises that are found in the file `src/bioinfo_exercises/algorithms.py`.
You can call the scripts in the `bin/` directory to evoke these functions.

Make sure you have the directory `src/` in your PYTHONPATH (in bash do `export PYTHONPATH="$PYTHONPATH:./src"`).
You can then call one of these scripts from the cloned directory like this `bin/mst -h`, which will print the usage for the script.


## Test Cases and Solutions

### Manual testing

There are a few test cases and solutions in `test/input/` and `test/solutions/`.
You could run your function on a test case by typing `bin/crossinggraph test/input/crossinggraph_input0.txt`.

### Automated testing

Your code for `getLongestSubstring` can be tested on the solutions directory automatically by typing `python -m unittest test.test_exercises.TestLCS.test_cases` at the top of this directory.
Automated comparison from the solutions directory for the other two is not implemented yet.

If you have the `bioinfo_exercises_solutions` package, you can run randomized
test cases by doing `python -m unittest test.test_exercises.TestLCS.test_cases`.
